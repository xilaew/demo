from flask import Flask, request
import json

app = Flask(__name__)

@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"

users = ["Kati", "Camilla", "Ole", "Mel"]

@app.route("/users")
def get_users():
    return json.dumps(users)

@app.route("/add")
def add():
    a = request.args.get('a')
    b = request.args.get('b')
    return str(int(a) + int(b))

# app.run(host='0.0.0.0', port=8080)
